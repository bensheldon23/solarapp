Rails.application.routes.draw do
  post 'pv_production/' => 'pv_production#pv_production'
  root to: 'home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
