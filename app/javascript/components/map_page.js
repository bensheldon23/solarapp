import React from 'react';
import SolarOutputs from "./solar_outputs";
import {Button, ControlLabel, Form, FormControl, FormGroup, Panel, PanelGroup, Well} from "react-bootstrap";
import SolarMap from "./map";

export default class MapPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            outputs: null,
            azimuth: "180",
            tilt: "15"
        }
        this.map = React.createRef();
        this.searchInput = React.createRef();
        this.updateOutputs = this.updateOutputs.bind(this);
    }
    drawPolygonHandler = () => {
        this.map.current.drawPolygon();
    }
    clearDrawHandler = () => {
        this.map.current.clearDraw();
    }
    render() {
        var solarOutputs = (<div></div>);
        if (this.state.outputs) {
            solarOutputs = (<SolarOutputs outputs={this.state.outputs}/>);
        }
        return (
            <div>
                <input ref={this.searchInput} id="search-input" className="controls" type="text" placeholder="Enter Address" />

                <div className="map_container">
                    <SolarMap ref={this.map}
                              updateOutputs={this.updateOutputs}
                              searchInput={this.searchInput}
                              tilt={this.state.tilt}
                              azimuth={this.state.azimuth}
                    />
                    <PanelGroup id="results_panel">
                        <Button className="map_button" onClick={this.drawPolygonHandler}>Draw Outline</Button>
                        <Button className="map_button" onClick={this.clearDrawHandler}>Clear</Button>
                        <PanelGroup id="inputs_panel">
                            <Panel eventKey="1" defaultExpanded>
                                <Panel.Heading className="panel_header">
                                    <Panel.Title toggle>Advanced Inputs</Panel.Title>
                                </Panel.Heading>
                                <Panel.Body className="panel_body" collapsible={true}>
                                    <Form inline>
                                        <FormGroup
                                            className="input_form"
                                            controlId="formAzimuth"
                                            validationState={this.getAzimuthValidation()}
                                        >
                                            <ControlLabel>Solar Azimuth</ControlLabel>
                                            <FormControl
                                                className="text_input"
                                                type="text"
                                                value={this.state.azimuth}
                                                placeholder="Enter text"
                                                onChange={this.handleAzimuth.bind(this)}
                                            />
                                        </FormGroup>
                                        <FormGroup
                                            className="input_form"
                                            controlId="formTilt"
                                            validationState={this.getTiltValidation()}
                                        >
                                            <ControlLabel>Panel Tilt</ControlLabel>
                                            <FormControl
                                                className="text_input"
                                                type="text"
                                                value={this.state.tilt}
                                                placeholder="Enter text"
                                                onChange={this.handleTilt.bind(this)}
                                            />
                                        </FormGroup>
                                    </Form>
                                </Panel.Body>
                            </Panel>
                        </PanelGroup>
                        {solarOutputs}
                    </PanelGroup>
                </div>
            </div>
            );
    }
    getAzimuthValidation() {
        var azimuth = parseFloat(this.state.azimuth);
        if (azimuth >= 0 && azimuth <= 360) {
            return 'success';
        }
        else return 'error';
    }
    handleAzimuth(e) {
        this.setState({ azimuth: e.target.value });
    }
    getTiltValidation() {
        var tilt = parseFloat(this.state.tilt);
        if (tilt >= 0 && tilt <= 45) {
            return 'success';
        }
        else return 'error';
    }
    handleTilt(e) {
        this.setState({ tilt: e.target.value });

    }
    updateOutputs(outputs) {
        this.setState({outputs: outputs});
    }
}