import React from 'react';

export default class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (<div id="about_div">
            <h2>Welcome to Solar Start!</h2>
            <br/>
            <h4>Solar Start was designed to provide an easy to use interface for sizing PV systems.</h4>
            <br/>
            <h4>1.) Start by entering an address into the search box and pressing enter or selecting one of the options.</h4>
            <img src={require('../../assets/images/step_1.jpg')} />
            <br/>
            <br/>
            <h4>2.) Then select the "Draw Outline" button on the right side of the screen.</h4>
            <img src={require('../../assets/images/step_2.jpg')} />
            <br/>
            <br/>
            <h4>3.) Starting on the perimeter of the building, click around the corners until you have returned to your starting point.</h4>
            <img src={require('../../assets/images/step_3.jpg')} />
            <br/>
            <br/>
            <h4>4.) Voila! On the right you will see a summary of the estimated characteristics of your system and a graph showing monthly energy production.</h4>
            <img src={require('../../assets/images/step_4.jpg')} />
            <br/>
            <br/>
            <h4>5.) You can adjust your outline and see updated results appear. Press escape, hit the clear button, or press draw outline again to start over. Enjoy!</h4>
        </div>);
    }
}