import React from 'react';
import SolarMap from '../components/map.js';
import { Panel, Nav, Navbar, NavItem, NavDropdown, MenuItem } from 'react-bootstrap'
import SolarOutputs from '../components/solar_outputs.js';
import About from '../components/about.js';
import MapPage from './map_page.js';
import { Route, NavLink, HashRouter, Link} from "react-router-dom";

export default class SolarApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <div>
                <HashRouter>
                    <div>
                        <Navbar inverse collapseOnSelect staticTop={true}>
                            <Navbar.Header>
                                <Nav>
                                    <NavItem id="logo_link" componentClass={Link} href="/" to="/" active={location.pathname === '/#'}>
                                        <img id="logo" src={require('../../assets/images/logo.png')} />
                                    </NavItem>
                                </Nav>
                                <Navbar.Toggle />
                            </Navbar.Header>
                            <Navbar.Collapse>
                                <Nav>
                                    <NavItem className="nav_item" componentClass={Link} href="/" to="/" active={location.pathname === '/#'}>
                                        Map
                                    </NavItem>
                                </Nav>
                                <Nav pullRight>
                                    <NavItem className="nav_item" componentClass={Link} href="/about" to="/about" active={location.pathname === '/about'}>
                                        About
                                    </NavItem>
                                </Nav>
                            </Navbar.Collapse>
                        </Navbar>
                        <Panel id="main_panel">
                            <div className="content">
                                <Route exact path="/" component={MapPage}/>
                                <Route path="/about" component={About}/>
                            </div>
                        </Panel>
                    </div>
                </HashRouter>
            </div>
        );
    }
}