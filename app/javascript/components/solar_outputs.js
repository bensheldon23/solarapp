import React from 'react';
import { Panel, PanelGroup, Table } from 'react-bootstrap'
import { BarChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar, ResponsiveContainer } from 'recharts';

export default class SolarOutputs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        var outputs = this.props.outputs;

        var graph_data = [];
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        for (var i=0; i < outputs.ac_monthly.length; i++) {
            graph_data.push({
                "name": months[i],
                "ac_monthly": outputs.ac_monthly[i]
            });
        }

        return (
            <div className="solar_outputs">
                <Panel className="outputs_panel" eventKey="1" defaultExpanded>
                    <Panel.Heading className="panel_heading">
                        <Panel.Title toggle>Summary</Panel.Title>
                    </Panel.Heading>
                    <Panel.Body className="panel_body" collapsible={true}>
                        <Table striped bordered condensed hover>
                            <tbody>
                                <tr>
                                    <td>Nominal Power (DC)</td>
                                    <td>{outputs.system_capacity.toLocaleString() + ' kW'}</td>
                                </tr>
                                <tr>
                                    <td>Yield (AC)</td>
                                    <td>{outputs.solar_yield.toLocaleString() + ' kWh / kW / year'}</td>
                                </tr>
                                <tr>
                                    <td>Annual Production (AC)</td>
                                    <td>{outputs.annual_production_ac.toLocaleString() + ' kWh'}</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Panel.Body>
                </Panel>
                <Panel className="outputs_panel" eventKey="2" defaultExpanded>
                    <Panel.Heading className="panel_heading">
                        <Panel.Title toggle>Monthly Results</Panel.Title>
                    </Panel.Heading>
                    <Panel.Body className="panel_body" collapsible={true}>
                        <ResponsiveContainer width="100%" height={200}>
                            <BarChart data={graph_data}>
                                <CartesianGrid strokeDasharray="3 3" />
                                <XAxis dataKey="name" tick={{ fill: '#C0C0C0' }}/>
                                <YAxis tick={{ fill: '#C0C0C0' }}/>
                                <Tooltip />
                                <Legend tick={{ fill: '#C0C0C0' }} />
                                <Bar dataKey="ac_monthly" fill="#00A86B" />
                            </BarChart>
                        </ResponsiveContainer>
                    </Panel.Body>
                </Panel>
            </div>
        );
    }
}