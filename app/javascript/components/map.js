import React from 'react'
import {Button, Form, FormGroup, ControlLabel, FormControl, HelpBlock, PanelGroup, Panel} from 'react-bootstrap'
import $ from 'jquery';

export default class SolarMap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            polygons: null,
        }
        this.setPolygonListeners = this.setPolygonListeners.bind(this);
    }
    componentDidMount() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 42.3815995, lng: -71.0788927 },
            tilt: 0,
            zoom: 15,
            mapTypeId: 'satellite',
            mapTypeControl: false,
            minZoom: 15,
            maxZoom: 300,
        });
        var polyOptions = {
            strokeWeight: 2.5,
            fillOpacity: 0.35,
            editable: true,
            strokeColor: '#00A86B',
            fillColor: '#FFBA00'
        };
        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.Polygon,
            drawingControl: false,

            polygonOptions: polyOptions
        });
        drawingManager.setMap(map);
        this.setState({map: map, drawingManager: drawingManager});
        google.maps.event.addListener(drawingManager,'polygoncomplete',this.drawComplete.bind(this));

        // Erase outline on ESCAPE
        google.maps.event.addDomListener(document, 'keyup', function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);

            if (code === 27) {
                this.clearDraw();
            }
        }.bind(this));

        var input = this.props.searchInput.current;
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls.push(input);

        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }


            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }
    componentDidUpdate(prevProps, prevState) {
        if ((prevProps.tilt != this.props.tilt) || (prevProps.azimuth != this.props.azimuth)) {
            this.getResults();
        }
    }
    drawPolygon() {
        this.clearDraw();

        // Draw new outline
        this.state.drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
    }
    drawComplete(polygon) {
        this.setState({polygon: polygon});
        this.state.drawingManager.setDrawingMode(null);
        this.setPolygonListeners(polygon);
        this.getResults();
    }
    clearDraw() {
        // Clear existing outline
        if (this.state.polygon != null) {
            this.state.polygon.setMap(null)
            this.setState({polygon: null})
            this.props.updateOutputs(null);
        }

        // Focus on map
        this.state.map.focus;
    }
    getResults() {
        if (this.state.polygon && this.props.azimuth && this.props.tilt)
        {
            var area = google.maps.geometry.spherical.computeArea(this.state.polygon.getPath());

            var bounds=new google.maps.LatLngBounds();
            this.state.polygon.getPath().forEach(function(latLng){
                //extend the bounds
                bounds.extend(latLng);
            });
            fetch('../pv_production/', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    lat: bounds.getCenter().lat(),
                    lon: bounds.getCenter().lng(),
                    area: area,
                    tilt: this.props.tilt,
                    azimuth: this.props.azimuth
                })})
                .then(response => response.json())
                .then(data => this.props.updateOutputs(data))
                .catch(function() {
                    console.log("error");
            });
        }
        else {
            this.props.updateOutputs(null)
        }
    }
    setPolygonListeners(polygon) {
        polygon.getPaths().forEach(function(path, index){
            var test = this.state.polygon;
            google.maps.event.addListener(path, 'insert_at', function(){
                // Point was removed
                this.getResults();
            }.bind(this));

            google.maps.event.addListener(path, 'remove_at', function(){
                // Point was removed
                this.getResults();
            }.bind(this));

            google.maps.event.addListener(path, 'set_at', function(){
                // Point was moved
                this.getResults();
            }.bind(this));
        }.bind(this));

        google.maps.event.addListener(polygon, 'dragend', function(){
            // Polygon was dragged
            this.getResults();
        }.bind(this));
    }
    render() {
        return (
            <div id="map" style={{height: 750}}></div>
        )
    }
};