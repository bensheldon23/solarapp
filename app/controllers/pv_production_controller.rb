require 'rest-client'
require 'json'

class PvProductionController < ApplicationController
  protect_from_forgery except: :pv_watts
  include PvWattsHelper

  def pv_production
    params = JSON.parse(request.raw_post)

    if ((not params["tilt"].numeric?) or  (params["tilt"].to_f < 0) \
        or (params["tilt"].to_f > 45))
      raise "Invalid tilt!"
    end

    if ((not params["azimuth"].numeric? ) or  (params["azimuth"].to_f < 0) \
        or (params["azimuth"].to_f > 360))
      raise "Invalid azimuth!"
    end

    inputs = {
        lat: params["lat"],
        lon: params["lon"],
        tilt: params["tilt"],
        azimuth: params["azimuth"]
    }

    # Assumin 80% of roof used, 260W 5.4864 m^2 modules
    area = params["area"] / Math.cos(params["tilt"].to_f * Math::PI / 180)
    inputs["system_capacity"] = floor2((0.6 * area / 5.4864) * 0.260,3)
    result = pv_watts(inputs)

    annual_production_ac = result["outputs"]["ac_annual"].floor
    solar_yield = annual_production_ac / inputs["system_capacity"]

    output = {
        "system_capacity": inputs["system_capacity"],
        "solar_yield": solar_yield,
        "annual_production_ac": annual_production_ac,
        "ac_monthly": result["outputs"]["ac_monthly"]
    }
    render json: output
  end

  def floor2(num, exp = 0)
    multiplier = 10 ** exp
    ((num * multiplier).floor).to_f/multiplier.to_f
  end

end

class String
  def numeric?
    return true if self =~ /\A\d+\Z/
    true if Float(self) rescue false
  end
end
