require 'json'

module PvWattsHelper
  def pv_watts(inputs)
    dict = inputs
    dict["api_key"] = "4CApCIIql1jaXnopl4vdu6J9ibKSuUd2HKQJjsgS"
    dict["timeframe"] = "monthly"
    dict["module_type"] = 1
    dict["losses"] = 10
    dict["array_type"] = 1

    url = "https://developer.nrel.gov/api/pvwatts/v5.json?"
    url += dict.to_param
    response = RestClient.get(url)
    if response.code == 200
      return JSON.parse(response.body)
    end
    raise "PVWatts API returned error"
  end
end
