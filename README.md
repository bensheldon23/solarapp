# README

### Local Setup

* Clone repository
    * From command line navigate to repo folder
    * "npm i"

* Development
    * "ruby ./bin/webpack-dev-server"
    * "rails s"
  
* Production
    * "bundle exec rake assets:precompile"
    * "rails s -e production"
    
### Third Party Libraries

* Below is a list of the primary third-party libraries/apis used in the app.
    * React.js
    * Webpacker
    * Recharts
    * React Bootstrap
    * PVWatts API
    * Google Maps API
  
### Assumptions

* Nominal Power
    * The area of the roof is determined by taking the area of the polygon
  and determining the area of the tilted roof using the equation
  Area_p = Area_f / cos(tilt).
    * The nominal power is determined by using the following equation:
        * Pn = Area_p * 0.6 (% usable) / 5.4864 (m^2 / module) * 0.260 (kW DC / module)
        * The module assumed is a Trina-260W.

* PVWatts
    * The PVWatts API is used to calculate the actual energy production of the site.
    * The latitude and longitude from the site as well as the entered tilt and azimuth are inputs.

